#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtSerialPort/QSerialPort>

//импорт пользовательского класса порта
#include "myport.h"

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  QQmlApplicationEngine engine;

  //создание экземпляра класса так, чтобы работать с ним как с синглтоном
  myPort *Serial = myPort::instance();
  //настройка порта
  Serial->monitoring();
  //запуск стрима
  Serial->tryConnect();

  //провязка c++ с qml. qml будет работать с экземпляром "Serial" для вывода новых данных в интерфейс
  qmlRegisterSingletonType<myPort>("myport", 1, 0, "MyPort", myPort::qmlInstance);

  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}

