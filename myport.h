#ifndef MYPORT_H
#define MYPORT_H

//режим отладки активирован (См. вывод приложения).
#define DEBUG_MODE_EN

// необходимо добавить в начало файла проекта .pro строчку "QT += serialport"
#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QQmlApplicationEngine>

//создание класса как наследника QObject, чтобы к нему был доступ из qml
class myPort : public QObject
{
  //дает возможность ко всем фишкам Qt
  Q_OBJECT
  //организация работы класса в qml: доступ к классу осуществляется через
  //свойство serialBuf, он возвращает массив принятых данных.
  //serialBufChanged - сигнал, по которому вызывается serialBuf.
  //READ serialBuf говорит о том, что в qml это чтение данных.
  Q_PROPERTY(QString dataBuf READ dataBuf WRITE write NOTIFY dataBufChanged)
public:
  explicit myPort(QObject *parent = nullptr);
  ~myPort();

  void tryConnect();
  void monitoring();
  void write(QString data);
  //метод для qml. Возвращает строку данных
  QString dataBuf();

  //статический метод для создания синглтона
  static myPort *instance(QObject *parent = nullptr) {
    static myPort m_myport(parent);
    return &m_myport;
  }

  //метод, который возвращает указатель на синглтон, который поймет qml
  static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    myPort *m_qmlInstance = instance();
    QQmlEngine::setObjectOwnership(m_qmlInstance, QQmlEngine::CppOwnership);
    return m_qmlInstance;
  }

private:
  QSerialPort Serial;
  QByteArray m_dataBuf;

signals:
  //сигнал для qml
  void dataBufChanged();

private slots:
  void readFromPort();
};

#endif // MYPORT_H
