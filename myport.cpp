#include "myport.h"
#include <qdebug.h>
#include "string.h"
#include <QThread>

myPort::myPort(QObject *parent) : QObject(parent), m_dataBuf(nullptr)
{

}

myPort:: ~myPort(){

}


void myPort::tryConnect()
{
  Serial.setPortName("COM10");
  if(Serial.open(QIODevice::ReadWrite) &&
     Serial.setBaudRate(57600) &&
     Serial.setDataBits(QSerialPort::Data8) &&
     Serial.setStopBits(QSerialPort::OneStop) &&
     Serial.setParity(QSerialPort::NoParity) &&
     Serial.setFlowControl(QSerialPort::NoFlowControl))
  {
      if(Serial.isOpen())
        {
          qDebug() << "Connect address port succes";
          qDebug() << &Serial;
          qDebug() << Serial.portName();
        }
      else
        {
          Serial.close();
        }
  }
  else
    {
      Serial.close();
    }
}

void myPort::readFromPort()
{
  m_dataBuf.clear();
  if(Serial.bytesAvailable() > 0)
  {
    m_dataBuf.append(Serial.readAll());
  }
#ifdef  DEBUG_MODE_EN
  qDebug() << m_dataBuf.data();
#endif
  emit dataBufChanged();
}

void myPort::monitoring()
{
  //читай сигнал-слоты Qt
  connect(&Serial, SIGNAL(readyRead()), this, SLOT(readFromPort()));
}

void myPort::write(QString data)
{
  QByteArray sData;
  sData.append(data);
  if (Serial.isOpen())
  {
    qint64 dataWeight = Serial.write(sData);
    if (dataWeight > 0)
    {
      qDebug() << "Сообщение отправлено.";
    }
    else
    {
      qDebug() << "Ошибка. Сообщение НЕ отправлено.";
    }
  }
  else
  {
    qDebug() << "Сообщение НЕ отправлено. Отсутствует открытый порт.";
  }
}

QString myPort::dataBuf()
{
  //qml нужна строка, поэтому данные конвертируются из массива в строку
  QString BufToStr;
  BufToStr.append(m_dataBuf);//QString::fromUtf16((ushort *)m_dataBuf.data());
  return BufToStr;
}
