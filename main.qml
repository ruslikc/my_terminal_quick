import QtQuick 2.9
import QtQuick.Window 2.2
//импорт пользовательского класса порта
import myport 1.0

Window {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    color: "#bfbfbf"
    title: qsTr("My Terminal")


    Text {
        id: portOutput
        height: 301
        text: ""
        styleColor: "#00000000"
        font.weight: Font.Medium
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        font.capitalization: Font.MixedCase
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.topMargin: 25
    }

    //обновление данных
    Connections {
        target: MyPort
        //по сигналу, объявленному в с++
        onDataBufChanged: portOutput.text += MyPort.dataBuf
    }

    Rectangle {
        id: start_btn
        x: 528
        y: 440
        width: 102
        height: 30
        color: "#ffffff"
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10

        Text {
            id: element
            x: 19
            y: 8
            text: qsTr("Отправить")
            anchors.horizontalCenter: mouseArea.horizontalCenter
            anchors.verticalCenter: mouseArea.verticalCenter
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 12
        }

        MouseArea {
            id: mouseArea
            x: 0
            y: 0
            width: 102
            height: 30
            onClicked: MyPort.dataBuf = portInput.text
        }
    }

    TextInput {
        id: portInput
        height: 45
        text: qsTr("abraCadabra")
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: portOutput.bottom
        anchors.topMargin: 25
        font.pixelSize: 12
    }

    Text {
        id: label1
        y: -53
        height: 15
        text: qsTr("Вывод порта")
        anchors.bottom: portOutput.top
        anchors.bottomMargin: 0
        anchors.left: portOutput.left
        anchors.leftMargin: 0
        font.pixelSize: 12
    }

    Text {
        id: label2
        y: 334
        height: 15
        text: qsTr("Ввод порта")
        anchors.left: portInput.left
        anchors.leftMargin: 0
        anchors.bottom: portInput.top
        anchors.bottomMargin: 0
        font.pixelSize: 12
    }

}



/*##^## Designer {
    D{i:2;anchors_height:200;anchors_width:200;anchors_x:79;anchors_y:61}D{i:5;anchors_x:19;anchors_y:8}
D{i:6;anchors_width:563;anchors_x:52;anchors_y:383}D{i:4;anchors_x:19;anchors_y:8}
D{i:8;anchors_height:15;anchors_width:200;anchors_x:"-71";anchors_y:334}D{i:7;anchors_width:563;anchors_x:254;anchors_y:383}
D{i:9;anchors_x:254}D{i:10;anchors_height:15;anchors_x:-71;anchors_y:334}
}
 ##^##*/
